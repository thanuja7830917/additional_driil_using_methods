function surnameWithA(got) {
    try{
        const surname_A=Object.entries(got).reduce((object,curr)=>{
            curr[1].map((persons)=>{
                    persons.people.map((each_person)=>{
                        const surname=each_person.name.split(" ")
                        if(surname[1].match(/^[Aa]/)){
                            object.push(each_person.name)
                        }
                    })
            })
            return object
        },[])
        return surname_A
    }
    catch(error){
        console.error(error)
    }
}
module.exports=surnameWithA