function nameWithS(got) {
    try{
        const each_person=Object.entries(got).reduce((object,curr)=>{
            curr[1].map((each)=>{
                each.people.map((person)=>{
                    if(person.name.match(/[Ss]/)){
                        object.push(person.name)
                    }
                })
            })
            return object
        },[])
        return each_person
    }
    catch(error){
        console.error(error)
    }
}
module.exports=nameWithS