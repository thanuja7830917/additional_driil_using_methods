function peopleNameOfAllHouses(got){
    try{
        const allnames=Object.entries(got).reduce((object,curr)=>{
                curr[1].map((each)=>{
                    const people_list=each.people.reduce((array,person)=>{
                        array.push(person.name)
                        return array
                    },[])
                    object[each.name]=people_list
                    
                })
                return object
        },{})
        const result=Object.entries(allnames).sort()
        return Object.fromEntries(result)
    }
    catch(error){
        console.error(error)
    }
}
module.exports=peopleNameOfAllHouses