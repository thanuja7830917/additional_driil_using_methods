function peopleByHouses(got) {
    try{
        const people_by_houses=Object.entries(got).reduce((object,each)=>{
            each[1].map((element)=>{
                if(object[element.name]){
                    object[element.name]+=element.people.length
                }
                else{
                    object[element.name]=element.people.length
                }
            })
            return object
        },{})
        return people_by_houses
    }
    catch(error){
        console.error(error)
    }
}
module.exports=peopleByHouses