function surnameWithS(got) {
    try{
        const surname_S=Object.entries(got).reduce((object,curr)=>{
            curr[1].map((persons)=>{
                persons.people.map((each_person)=>{
                    const surname=each_person.name.split(" ")
                    if(surname[1].match(/^[Ss]/)){
                        object.push(each_person.name)
                    }
                })
                
            })
            return object
        },[])
        return surname_S
    }
    catch(error){
        console.error(error)
    }
}

module.exoprts=surnameWithS